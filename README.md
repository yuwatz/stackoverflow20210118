# How to reproduction

1. Clone this repository.
2. Install dependencies.
```
cd stackoverflow20210118
npm install # or yarn install
```

3. build "lib" package
```
cd stackoverflow20210118/lib
npm run build
```

4. start "web" package
```
cd stackoverflow20210118/web
npm run start
```

5. Start devtools on the automatically opened page.
