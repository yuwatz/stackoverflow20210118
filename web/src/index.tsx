import React from 'react';
import ReactDOM from 'react-dom';
import * as lib from 'lib'; // my library project.

const el = document.getElementById('root');
// Use custom input component.
ReactDOM.render(<lib.CustomInput />, el);

