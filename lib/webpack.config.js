const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin");
module.exports = {
    mode: "development",
    entry: "./src/index.ts",
    output: {
        path: `${__dirname}/dist`,
        filename: "index.js",
        library: 'LIB',
        libraryTarget: 'var',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [{ loader: 'babel-loader' }, { loader: 'ts-loader' }],
            },
        ],
    },
    plugins: [new EsmWebpackPlugin()],
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
    },
};
